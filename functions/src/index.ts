import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import { MCTRequest, MCTData, MCTResponseData, MCTResponse } from './mct-modal.interface'

admin.initializeApp();
export const GetMCTString = functions.https.onRequest((request, response) => {
    let mctrequest = {} as MCTRequest;
    console.log(request.query);
    mctrequest.arrivalCarrier = request.query.arrivalCarrier;
    mctrequest.arrivalDate = request.query.arrivalDate;
    mctrequest.arrivalFlight = request.query.arrivalFlight;
    mctrequest.arrivalSegmentOrigin = request.query.arrivalSegmentOrigin;
    mctrequest.arrivalStation = request.query.arrivalStation;
    mctrequest.arrivalTime = request.query.arrivalTime;
    mctrequest.departureCarrier = request.query.departureCarrier;
    mctrequest.departureDate = request.query.departureDate;
    mctrequest.departureFlight = request.query.departureFlight;
    mctrequest.departureSegmentDestination = request.query.departureSegmentDestination;
    mctrequest.departureStation = request.query.departureStation;
    mctrequest.departureTime = request.query.departureTime;
    console.log(mctrequest);

    const mctList = admin.firestore().collection('mct1');
    mctList.where('arrivalCarrier', '==', mctrequest.arrivalCarrier)
    .where('arrivalDate', '==', mctrequest.arrivalDate)
    .where('arrivalFlight', '==', mctrequest.arrivalFlight)
    .where('arrivalSegmentOrigin', '==', mctrequest.arrivalSegmentOrigin)
    .where('arrivalStation', '==', mctrequest.arrivalStation)
    .where('arrivalTime', '==', mctrequest.arrivalTime)
    .where('departureCarrier', '==', mctrequest.departureCarrier)
    .where('departureDate', '==', mctrequest.departureDate)
    .where('departureFlight', '==', mctrequest.departureFlight)
    .where('departureSegmentDestination', '==', mctrequest.departureSegmentDestination)
    .where('departureStation', '==', mctrequest.departureStation)
    .where('departureTime', '==', mctrequest.departureTime)
    .get()
    .then(docList => {
        if (docList.empty || docList.size !== 1) {
            console.log("No such document or more than 1 doc!:" + docList);
            response.status(404).send(null);
        } else {
            docList.forEach(doc => {
                console.log('data matched');
                console.log(doc.data());
                let mctresponsedata = {} as MCTResponseData;
                mctresponsedata.ID = doc.data().ID;
                mctresponsedata.action= doc.data().action;
                mctresponsedata.connect = doc.data().connect;
                mctresponsedata.timeint = doc.data().timeint;
                mctresponsedata.timestr = doc.data().timestr;
                const mctresponse: MCTResponse = {MCT: mctresponsedata};
                response.send(mctresponse);
                return;
            });
        }
    })
    .catch(error => {
        console.log(error);
        response.status(500).send(error);
    })
});

export const createMCT = functions.https.onRequest((request, response) => {
    let mctdata = {} as MCTData;
    console.log(request.body);
    console.log(request.method);
    mctdata = request.body
    admin.firestore().collection('mct1').doc().set(mctdata)
    .then(res => {
        console.log(res);
        response.send(res);
    })
    .catch(err => {
        console.log(err);
        response.send(err);
    })
});
