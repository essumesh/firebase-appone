export interface MCTRequest {
    arrivalCarrier: string;
    arrivalFlight: string;
    arrivalDate: string;
    arrivalTime: string;
    arrivalStation: string;
    arrivalSegmentOrigin: string;
    departureCarrier: string;
    departureFlight: string;
    departureDate: string;
    departureTime: string;
    departureStation: string;
    departureSegmentDestination: string;
}

export interface MCTResponseData {
    connect: string;
    timestr: string;
    timeint: string;
    action: string;
    ID: string;
}

export interface MCTResponse {
    MCT: MCTResponseData;
}

export interface MCTData {
    arrivalCarrier: string;
    arrivalFlight: string;
    arrivalDate: string;
    arrivalTime: string;
    arrivalStation: string;
    arrivalSegmentOrigin: string;
    departureCarrier: string;
    departureFlight: string;
    departureDate: string;
    departureTime: string;
    departureStation: string;
    departureSegmentDestination: string;
    connect: string;
    timestr: string;
    timeint: string;
    action: string;
    ID: string;
}